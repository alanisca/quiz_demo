var quizArray;
// var replacePregunta;
$(window).load(function()  {
	$.ajax({
		type: 'GET',
		// data: {'id' : $(this).data('id')},
		url: `http://localhost:3000/quiz/getQuiz/${$(".mainGame").attr("data-id")}`,
		dataType: 'html',
		success: function (data) {
			quizArray = JSON.parse(data);
			console.log(quizArray);
			initElements();
		}
	});
});
function initElements(){
	crearEjercicios();
}
function crearEjercicios(){
	var estrucutraEjercicio;
	$('.mainGame').append('<div class="contenedorEjercicios"></div>');
	for(var i = 0; i < quizArray.reactivos.length;i++){
		estrucutraEjercicio = '';
		estrucutraEjercicio += 	'<div class=contenedorEjercicio data-ejercicio="'+(i+1)+'">'+
									'<div class="textoPregunta">'+quizArray.reactivos[i].pregunta+''+
										'<div class="contenedorSelect">'+
											'<select>';
												for(var a = 0; a < quizArray.reactivos[i].respuestas.length; a++){
													console.log(quizArray.reactivos[i].respuestas[a]);
													estrucutraEjercicio +=	'<option class="respuestaEjercicio" value="'+quizArray.reactivos[i].respuestas[a].respuestaOpcion+'">'+quizArray.reactivos[i].respuestas[a].textoOpcion+'</option>';
												}
					estrucutraEjercicio += 	'</select>'+
										'</div>'+
									'</div>'+
								'</div>';
		$('.contenedorEjercicios').append(estrucutraEjercicio);
	}
	$('.contenedorEjercicios').append('<div class="botonValidar">Revisar</div>');
	$('.botonValidar').on(clickHandler, function(){
	    validarRespuestas();
	});
}

function obtenerPregunta(index){
	replacePregunta = '';
	replacePregunta += 	'<div class="contenedorSelect">'+
							'<select>';
								for(var i = 0; i < quizArray[index].reactivos.length;i++ ){
									replacePregunta += '<option class="respuestaEjercicio" value="'+quizArray[index].reactivos[i].respuestaOpcion+'">'+quizArray[index].reactivos[i].textoOpcion+'</option>';
								}
		replacePregunta +=	'</select>'+
							'<i></i>'+
						'</div>';
}
function validarRespuestas(){
	$('.contenedorEjercicio select').each(function(){
		if( $(this).val() == 'true' ){
			TweenMax.to( $(this), 0.4 , { boxShadow: '0px 0px 6px 5px green', yoyo: true, repeat: 1, ease: Linear.easeNone});
		}
		else{
			TweenMax.to( $(this), 0.4 , { boxShadow: '0px 0px 6px 5px red', yoyo: true, repeat: 1, ease: Linear.easeNone});
		}
	});
}