var quizArray;
$(window).load( function()  {
	$.ajax({
		type: 'GET',
		url: `/quiz/getQuizes`,
		dataType: 'html',
		success: function (data) {
			quizArray = JSON.parse(data);
			initTabla();
		}
	});
});
function initTabla(){
    crearFilas();
}
function crearFilas(){
    let estructuraFilas = '';
    for(var i = 0; i <quizArray.length; i++){
        estructuraFilas = 	`<tr class="filaTabla" data-id="${quizArray[i]._id}">
								<td class="celdas">Quiz demo</td>
								<td class="celdas">${quizArray[i].titulo}</td>
								<td class="celdas">${(quizArray[i].materia ? quizArray[i].materia : "No definido")}</td>
								<td class="celdas">${(quizArray[i].grado ? quizArray[i].grado : "No definido")}</td>
								<td class="celdas celdaVer"></td>
								<td class="celdas celdaEditar"></td>
								<td class="celdas celdaEliminar"></td>
							</tr>`;
		$('.tablaQuiz').append(estructuraFilas);
    }
}
$(".tablaQuiz").on(clickHandler, ".celdaVer", function(){
	const idQuiz = $(this).parents(".filaTabla").attr("data-id");
	window.location.href = `/quiz/quiz/${idQuiz}`;
})
$(".tablaQuiz").on(clickHandler, ".celdaEditar", async function(){
	const idQuiz = $(this).parents(".filaTabla").attr("data-id");
	window.location.href = `/quiz/editQuiz/${idQuiz}`;
})
$(".tablaQuiz").on(clickHandler, ".celdaEliminar", async function(){
	const idQuiz = $(this).parents(".filaTabla").attr("data-id");
	try {
        // https://developer.mozilla.org/es/docs/Web/API/HTMLElement/dataset
        const data = await fetch(`/quiz/quizdelete/${idQuiz}`, {
          method: 'delete'
        })
        const res = await data.json()
        console.log(res)
        if(res.estado){
            window.location.href = '/'
        }else{
            console.log(res)
        }
    } catch (error) {
        console.log(error)
    }
})