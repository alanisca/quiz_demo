$(window).load(async function(){
    const idQuiz = $(".contenedorDatos").attr("data-id");
    console.log(idQuiz);
    if (idQuiz != 0) {
        $("button.guardarQuiz").attr("data-accion","editar");
        $("button.guardarQuiz").text("Modificar");
        try {
            $.ajax({
                type: 'GET',
                // data: {'id' : $(this).data('id')},
                url: `/quiz/getQuiz/${idQuiz}`,
                dataType: 'html',
                success: function (data) {
                    quizArray = JSON.parse(data);
                    console.log(quizArray.titulo);
                    $("input.datoTitulo").val(quizArray.titulo);
                    $("input.datoMateria").val(quizArray.materia);
                    $("input.datoGrado").val(quizArray.grado);
                    for (let i = 0; i < quizArray.reactivos.length; i++) {
                        console.log(quizArray.reactivos[i].pregunta);
                        console.log(quizArray.reactivos[i].respuestas);
                        crearReactivo(quizArray.reactivos[i].pregunta, quizArray.reactivos[i].respuestas);
                        for (let j = 0; j < quizArray.reactivos[i].respuestas.length; j++) {
                            // insertReactivo(i,j);
                            console.log(`opcion: ${quizArray.reactivos[i].respuestas[j].opcion}` );
                            console.log(`textoOpcion: ${quizArray.reactivos[i].respuestas[j].textoOpcion}` );
                            console.log(`respuestaOpcion: ${quizArray.reactivos[i].respuestas[j].respuestaOpcion}` );
                        }
                    }
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
    else{
        $("button.guardarQuiz").attr("data-accion","nuevo");
        crearReactivo();
    }
});
function crearReactivo(preguntaBD = "", reactivoBD = [{opcion:1,textoOpcion:"",respuestaOpcion:false}]){
    const pregunta = $('.contenedorFormulario .contenedorReactivo').length;
    let estructuraReactivo = `<div class="contenedorReactivo" data-pregunta="${pregunta}">
                                <button class="addPregunta" type="button">+</button>
                                <div class="contenedorPregunta">
                                    <div class="elementoPregunta contenedorTexto">Pregunta:</div>
                                    <div class="elementoPregunta contenedorInput">
                                        <input class="inputPregunta" type="text" placeholder="Agregar pregunta" name="p_${pregunta}" value="${preguntaBD}">
                                    </div>
                                </div>
                                <div class="contenedorOpciones">
                                    ${insertReactivo(reactivoBD)}
                                </div>
                            </div>`;
    $('.contenedorFormulario').append(estructuraReactivo);
}
function insertReactivo(arrObj){
    var estrucutraOpcion = "";
    for (let i = 0; i < arrObj.length; i++) {
        var estrucutraOpcion =  `${estrucutraOpcion}<div class="contenedorOpcion">
                                    <div class="elementoOpcion contenedorTexto">Reactivo ${arrObj[i].opcion}:</div>
                                    <div class="elementoOpcion contenedorInput">
                                        <input type="text" placeholder="Agregar reactivo" class="reactivo inputOpcion" data-reactivo="${arrObj[i].opcion}" value="${arrObj[i].textoOpcion}">
                                    </div>
                                    <div class="elementoOpcion contenedorCheck">
                                                <input class="inputCheck" type="checkbox" ${(arrObj[i].respuestaOpcion ? "checked" : "")}>
                                            </div>
                                    <div class="elementoOpcion contenedorBoton">
                                        <button type="button" class="addReactivo">+</button>
                                    </div>
                                </div>`;
        
    }
    return estrucutraOpcion;
}
$(".contenedorFormulario").on("click",".addReactivo", function(){
    const pregunta = $(this).parents(".contenedorReactivo").attr("data-pregunta");
    const dataReactivo = $(this).parents('.contenedorOpciones').children('.contenedorOpcion').length + 1;
    let estrucutraOpcion =  insertReactivo( reactivoBD = [{opcion:dataReactivo,textoOpcion:"",respuestaOpcion:false}]  );
    $(this).parents('.contenedorOpciones').append(estrucutraOpcion);
    $(this).css('display','none');
});
$(".contenedorFormulario").on("click",".addPregunta", function(){
    crearReactivo( );
    $(this).css('display','none');
});

const data = new Object;
$("#enviar").on("click", function(){
    data.titulo = $('.inputDatos.datoTitulo').val();
    data.materia = $('.inputDatos.datoMateria').val();
    data.grado = $('.inputDatos.datoGrado').val();
    data.reactivos = [];
    $('.contenedorReactivo').each(function(indexReactivo){
        var objReactivo = $(this);
        data.reactivos[indexReactivo] = new Object();
        data.reactivos[indexReactivo].pregunta = objReactivo.find('.inputPregunta').val();
        data.reactivos[indexReactivo].respuestas  = [];
        objReactivo.find('.contenedorOpcion').each(function(indexOpcion){
            var objOpcion = $(this);
            data.reactivos[indexReactivo].respuestas[indexOpcion] = {};
            data.reactivos[indexReactivo].respuestas[indexOpcion].opcion = indexOpcion+1;
            data.reactivos[indexReactivo].respuestas[indexOpcion].textoOpcion = objOpcion.find('.inputOpcion').val();
            if( objOpcion.find('.inputCheck').prop('checked') ){
                data.reactivos[indexReactivo].respuestas[indexOpcion].respuestaOpcion = true;
            }
            else{
                data.reactivos[indexReactivo].respuestas[indexOpcion].respuestaOpcion = false;
            }

        });
    });
    const accionQuiz = $(this).attr("data-accion");
    var urlQuiz = "";
    if (accionQuiz == "editar") {
        const urlQuiz = `/editQuiz/${$(".contenedorDatos").attr("data-id")}`;
    }
    if (accionQuiz == "nuevo") {
        const urlQuiz = `/quiz/newQuiz`;
    }
    $.ajax({
        url : urlQuiz,
        data : {"data":JSON.stringify(data)}, //Enviar el JSON data como string dentro del parametro data para evitar deformación
        method : 'post', //en este caso
        dataType : 'JSON',
        success : function(response){
            if(response.estado){
                window.location.href = '/quiz'
            }
            else{
                console.log(response.mensaje);
            }
        },
        error: function(error){
            console.log("Error al guardar: "+error);
               //codigo error
        }
    });
});
