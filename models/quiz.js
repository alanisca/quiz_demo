const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const quizSchema = new Schema({
    titulo: String,
    materia: String,
    grado: String,
    reactivos: [{
        pregunta: String,
        respuestas: [{
            opcion:Number,
            textoOpcion:String,
            respuestaOpcion:Boolean
        }]
    }
    ],
});
const Quiz = mongoose.model('quiz_collections', quizSchema);
module.exports = Quiz;