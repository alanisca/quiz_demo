const express = require("express");
const bodyParser = require("body-parser");
const app = express();

require('dotenv').config();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// Puerto dinamico
const port = process.env.PORT || 3000;

app.set("view engine", "ejs");
app.set("views", __dirname + "/views");
app.use(express.static(__dirname + "/public"));

//Base de datos
const mongoose = require('mongoose');
// const user = process.env.USER;
// const password = process.env.PASSWORD;
// const dbName = process.env.DBNAME;

const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@quiz.skbfn.mongodb.net/${process.env.DB_DBNAME}?retryWrites=true&w=majority`;
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(()=> console.log('conectado a mongodb')) 
  .catch(e => console.log('Error de conexión:', e));


// Rutas web
app.use('/', require('./router/routerWeb'));
app.use('/quiz', require('./router/quizRouter'));


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});