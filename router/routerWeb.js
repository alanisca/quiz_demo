const express = require('express');
const router = express.Router();

const Quiz = require("../models/quiz");
router.get("/", async(req, res) => {
    try {
        const arrayQuiz = await Quiz.find().select("_id").select("titulo");
        console.log({"quizes":arrayQuiz});
        res.render("index",{"quizes":arrayQuiz});
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.post('/',async (req, res) => {
    const body = req.body;
    console.log(body);
});
module.exports = router;