const express = require('express');
const router = express.Router();

const Quiz = require("../models/quiz");
router.get("/", async (req, res) => {
    var arrayQuiz;
    try {
        arrayQuiz = await Quiz.find();
        console.log(arrayQuiz);
    } catch (error) {
        console.log("ERROR: "+error);
    }
    res.render("index", {
        quizArray: arrayQuiz
    });
});
router.get("/getQuizes", async(req, res) => {
    try {
        arrayQuiz = await Quiz.find().select("_id").select("titulo").select("grado").select("materia");
        console.log(arrayQuiz);
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify(arrayQuiz));
        res.end();
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.get('/newQuiz',(req,res) => {
    res.render('newQuiz',{"_id":0});
    console.log("new");
});
router.get('/editQuiz/:id', async(req,res) => {
    const id = req.params.id;
    try {
        const arrayQuiz = await Quiz.findOne({ "_id":id });
        res.render('newQuiz',arrayQuiz);
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.get('/getQuiz/:id', async(req,res) => {
    const id = req.params.id;
    try {
        const arrayQuiz = await Quiz.findOne({"_id":id });
        // console.log(arrayQuiz);
        res.writeHead(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify(arrayQuiz));
        res.end();
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.get('/quiz/:id', async(req,res) => {
    const id = req.params.id;
    try {
        const arrayQuiz = await Quiz.findOne({"_id":id }).select("_id");
        console.log(arrayQuiz);
        res.render('quiz',arrayQuiz);
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.delete('/quizDelete/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const arrayQuiz = await Quiz.findByIdAndDelete({ _id: id });
        console.log(arrayQuiz)
        if (!arrayQuiz) {
            res.json({
                estado: false,
                mensaje: 'No se puede eliminar'
            })
        } else {
            res.json({
                estado: true,
                mensaje: 'eliminado!'
            })
        }
    } catch (error) {
        console.log(error)
    }
})
router.post('/newQuiz', async (req, res) => {
    const body = (req.body);
    try {
        const quizDB = new Quiz(JSON.parse(body.data));
        await quizDB.save();
        if (!quizDB) {
            res.json({
                estado: false,
                mensaje: 'No se puede crear'
            })
        } else {
            res.json({
                estado: true,
                mensaje: 'Creado!'
            })
        }
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
router.post('/editQuiz/:id', async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const quizDB = await Quiz.findByIdAndUpdate(
            id, JSON.parse(body.data), { useFindAndModify: false }
        )
        if (!quizDB) {
            res.json({
                estado: false,
                mensaje: 'No se puede editar'
            })
        } else {
            res.json({
                estado: true,
                mensaje: 'Editado!'
            })
        }
    } catch (error) {
        console.log("ERROR: "+error);
    }
});
module.exports = router;